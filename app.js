
// npm i express dotenv ioredis

import express from 'express'
// import { sendMessageToRedis } from './controller.js'
import { makeOrder } from './controllers/orderController.js'
import RedisConfig from './config.js'
import ccxt from 'ccxt'

const app = express()

app.use(express.json())

// app.post('/api/send', sendMessageToRedis)

app.all('/order', makeOrder)

// app.get('/', async (req, res) => {
//     // res.send(ccxt.exchanges)
//     let binance = new ccxt.binance()
//     // console.log (binance.id,  )
//     let theExchange = 'binance'
//     let theMethod = 'fetchTicker';
//     let tkr = await eval(`${theExchange}.${theMethod}('BTC/USD')`);
//     res.send(tkr)
//   }
// )

const redisconfig = new RedisConfig()

redisconfig.consume('create:blog', (message) => {
    console.log('Received message: ', message)
})

redisconfig.consume('order-agent:order', (json) => {
  console.log('order-agent -> order : ', json)
})

app.listen(process.env.PORT)

