
import RedisConfig from './config.js'

export const sendMessageToRedis = async (req, res) => {
    try {
        const { message } = req.body
        const redisconfig = new RedisConfig()
        redisconfig.produce('create:blog', message)
        return res
            .status(200)
            .json({ msg: 'Message successfully sent.'})
    } catch (err) {
        return console.log(err)
    }
}
