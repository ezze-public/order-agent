
import { Redis } from 'ioredis'
import dotenv from 'dotenv'
dotenv.config()

class RedisConfig {

    constructor() {
        this.redis = new Redis({
            host: process.env.REDIS_HOST,
            port: process.env.REDIS_PORT,
            password: process.env.REDIS_PASS,
        })
    }

    async consume( channel, callback ){
        await this.redis.subscribe(channel)
        this.redis.on('message', async (ch, message) => {
            if (channel == ch) {
                await callback(message)
            }
        })
    }

    async produce( channel, message ) {
        console.log('publishing "'+channel+'" with message of "'+message+'"')
        await this.redis.publish(channel, message)
    }

}

export default RedisConfig
